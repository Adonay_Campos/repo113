package com.taller.util;

import java.util.List;
import javax.ejb.Local;

@Local
public interface Dao<T> {

    public void create(T e);

    public void remove(T e);

    public void edit(T e);

    public T find(Object e);

    public List<T> findAll();

}
