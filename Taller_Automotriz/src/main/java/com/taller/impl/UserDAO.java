package com.taller.impl;

import com.taller.models.User;
import com.taller.util.Dao;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class UserDAO extends AbstractFacade<User> implements Dao<User> {

    private EntityManager em;

    public UserDAO(Class user) {
        super(user);
        em = Persistence.createEntityManagerFactory("persistencia").createEntityManager();
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
    
        public User iniciar(String us, String pass) {
        User user = null;
        String consulta;
        try {
            consulta = "SELECT u.id FROM Users as u WHERE u.user=? and u.password = aes_encrypt(?,'afe')";
            Query q = em.createNativeQuery(consulta);
            q.setParameter(1, us);
            q.setParameter(2, pass);
            List<User> list = q.getResultList();
            if (list.size() > 0) {
                String consulta2 = "SELECT u FROM User u WHERE u.user=?1 ";
                Query q1 = em.createQuery(consulta2);
                q1.setParameter(1, us);
                List<User> list2 = q1.getResultList();
                user=list2.get(0);
            }
        } catch (Exception e) {
            System.out.println("Error en: " + e.getMessage());
            e.printStackTrace();
        }
        return user;
    }
}
