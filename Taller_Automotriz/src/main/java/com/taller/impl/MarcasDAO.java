package com.taller.impl;

import com.taller.models.Marcas;
import com.taller.util.Dao;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class MarcasDAO extends AbstractFacade<Marcas> implements Dao<Marcas> {

    private EntityManager em;

    public MarcasDAO(Class marcas) {
        super(marcas);
        em = Persistence.createEntityManagerFactory("persistencia").createEntityManager();
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
}
