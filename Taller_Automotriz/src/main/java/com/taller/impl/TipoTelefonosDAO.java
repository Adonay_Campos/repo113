package com.taller.impl;

import com.taller.models.TipoTelefonos;
import com.taller.util.Dao;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class TipoTelefonosDAO extends AbstractFacade<TipoTelefonos> implements Dao<TipoTelefonos> {

    private EntityManager em;

    public TipoTelefonosDAO(Class tipoTelefonos) {
        super(tipoTelefonos);
        em = Persistence.createEntityManagerFactory("persistencia").createEntityManager();
    }

    public EntityManager getEntityManager() {
        return em;
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

}
