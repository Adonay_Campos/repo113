package com.taller.impl;

import com.taller.models.Empresa;
import com.taller.util.Dao;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class EmpresaDAO extends AbstractFacade<Empresa> implements Dao<Empresa> {

    private EntityManager em;
    
    public EmpresaDAO(Class empresa) {
        super(empresa);
         em = Persistence.createEntityManagerFactory("persistencia").createEntityManager();
    }

    public EntityManager getEntityManager() {
        return em;
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

}
