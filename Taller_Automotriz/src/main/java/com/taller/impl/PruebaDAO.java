/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taller.impl;

import com.taller.models.Prueba;
import com.taller.util.Dao;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author fredy.romerousam
 */
public class PruebaDAO extends AbstractFacade<Prueba> implements Dao<Prueba>{
    private EntityManager em;

    public PruebaDAO(Class prueba) {
        super(prueba);
        em = Persistence.createEntityManagerFactory("persistencia").createEntityManager();
    }

     @Override
    protected EntityManager getEntityManager() {
      return em;
    }
   
}
