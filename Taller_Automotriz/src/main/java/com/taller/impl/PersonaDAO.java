package com.taller.impl;

import com.taller.models.Persona;
import com.taller.util.Dao;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class PersonaDAO extends AbstractFacade<Persona> implements Dao<Persona>{
    
    private EntityManager em;

    public PersonaDAO(Class persona) {
        super(persona);
        em = Persistence.createEntityManagerFactory("persistencia").createEntityManager();
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }
}
