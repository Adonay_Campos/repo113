package com.taller.impl;

import com.taller.models.Autos;
import com.taller.util.Dao;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class AutosDAO extends AbstractFacade<Autos> implements Dao<Autos> {

    private EntityManager em;

    public AutosDAO(Class autos) {
        super(autos);
        em = Persistence.createEntityManagerFactory("persistencia").createEntityManager();
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
}
