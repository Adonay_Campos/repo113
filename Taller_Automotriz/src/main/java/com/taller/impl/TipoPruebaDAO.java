/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taller.impl;

import com.taller.models.TipoPrueba;
import com.taller.util.Dao;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author fredy.romerousam
 */
public class TipoPruebaDAO extends AbstractFacade<TipoPrueba> implements Dao<TipoPrueba> {

    private EntityManager em;

    public TipoPruebaDAO(Class tipoPrueba) {
        super(tipoPrueba);
        em = Persistence.createEntityManagerFactory("persistencia").createEntityManager();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

 
}
