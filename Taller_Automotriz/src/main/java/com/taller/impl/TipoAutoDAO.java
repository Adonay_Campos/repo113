package com.taller.impl;

import com.taller.models.TipoAutos;
import com.taller.util.Dao;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author erick.gomezusam
 */
public class TipoAutoDAO extends AbstractFacade<TipoAutos> implements Dao<TipoAutos>{

private EntityManager em;

    public TipoAutoDAO(Class tipoautos) {
        super(tipoautos);
        em = Persistence.createEntityManagerFactory("persistencia").createEntityManager();
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }   
}
