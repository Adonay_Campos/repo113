package com.taller.impl;

import com.taller.models.PersonaTelefono;
import com.taller.util.Dao;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class PersonaTelefonoDAO extends AbstractFacade<PersonaTelefono> implements Dao<PersonaTelefono> {

    private EntityManager em;

    public PersonaTelefonoDAO(Class personaTelefono) {
        super(personaTelefono);
        em = Persistence.createEntityManagerFactory("persistencia").createEntityManager();
    }

    public EntityManager getEntityManager() {
        return em;
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

}
