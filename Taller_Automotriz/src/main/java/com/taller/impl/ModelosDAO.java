package com.taller.impl;

import com.taller.models.Modelos;
import com.taller.util.Dao;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class ModelosDAO extends AbstractFacade<Modelos> implements Dao<Modelos> {

    private EntityManager em;

    public ModelosDAO(Class modelos) {
        super(modelos);
        em = Persistence.createEntityManagerFactory("persistencia").createEntityManager();
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
}
