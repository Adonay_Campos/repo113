package com.taller.impl;

import com.taller.models.PersonaCliente;
import com.taller.util.Dao;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class PersonaClienteDAO extends AbstractFacade<PersonaCliente> implements Dao<PersonaCliente>{
    
    private EntityManager em;

    public PersonaClienteDAO(Class personacliente) {
        super(personacliente);
        em = Persistence.createEntityManagerFactory("persistencia").createEntityManager();
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }
}
