/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taller.impl;

import com.taller.models.Refacciones;
import com.taller.util.Dao;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 *
 * @author fredy.romerousam
 */
public class RefaccionesDAO extends AbstractFacade<Refacciones> implements Dao<Refacciones> {

    private EntityManager em;

    public RefaccionesDAO(Class refaccion) {
        super(refaccion);
        em = Persistence.createEntityManagerFactory("persistencia").createEntityManager();
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
}
