package com.taller.impl;

import com.taller.models.Telefonos;
import com.taller.util.Dao;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class TelefonosDAO extends AbstractFacade<Telefonos> implements Dao<Telefonos> {

    private EntityManager em;

    public TelefonosDAO(Class telefonos) {
        super(telefonos);
        em = Persistence.createEntityManagerFactory("persistencia").createEntityManager();
    }

    public EntityManager getEntityManager() {
        return em;
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

}
