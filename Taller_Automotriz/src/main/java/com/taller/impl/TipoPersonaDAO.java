package com.taller.impl;

import com.taller.models.TipoPersona;
import com.taller.util.Dao;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

public class TipoPersonaDAO extends AbstractFacade<TipoPersona> implements Dao<TipoPersona>{
    
    private EntityManager em;

    public TipoPersonaDAO(Class tipopersona) {
        super(tipopersona);
        em = Persistence.createEntityManagerFactory("persistencia").createEntityManager();
    }

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }
}
