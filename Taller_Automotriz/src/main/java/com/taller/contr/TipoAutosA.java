/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taller.contr;

import com.taller.impl.TipoAutoDAO;
import com.taller.models.TipoAutos;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

/**
 *
 * @author erick.gomezusam
 */
@Named(value = "tipoAutosA")
@ManagedBean
@SessionScoped
public class TipoAutosA implements Serializable {

    private TipoAutoDAO tipoautosDAO= new TipoAutoDAO(TipoAutos.class);
    private TipoAutos tipoautos;
    private List<TipoAutos> listaTAutos;
    String mensaje="";

    public TipoAutos getTipoautos() {
        return tipoautos;
    }

    public void setTipoautos(TipoAutos tipoautos) {
        this.tipoautos = tipoautos;
    }

    public List<TipoAutos> getListaTAutos() {
        return listaTAutos;
    }

    public void setListaTAutos(List<TipoAutos> listaTAutos) {
        this.listaTAutos = listaTAutos;
    }

    public TipoAutoDAO getTipoautosDAO() {
        return tipoautosDAO;
    }

    public void setTipoautosDAO(TipoAutoDAO tipoautosDAO) {
        this.tipoautosDAO = tipoautosDAO;
    }
    
    
    @PostConstruct
    public void init() {
        this.tipoautos = new TipoAutos();
    }

    public void crear() {
        try {
           tipoautosDAO.create(tipoautos);
            this.mensaje = "Datos creados con exito";
        } catch (Exception e) {
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void editar() {
        try {
         tipoautosDAO.edit(tipoautos);
            this.mensaje = "Datos editados con exito";
        } catch (Exception e) {
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void eliminar(TipoAutos ta) {
        try {
          tipoautosDAO.remove(ta);
            this.mensaje = "Datos eliminados con exito";
        } catch (Exception e) {
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }public void cargarMarcas(TipoAutos ta){
        this.tipoautos=ta;
    }
}
