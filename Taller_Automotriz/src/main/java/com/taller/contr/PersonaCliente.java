package com.taller.contr;

import com.taller.impl.PersonaClienteDAO;
import com.taller.impl.PersonaDAO;
import com.taller.models.Persona;
import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named(value = "personaCliente")
@SessionScoped
public class PersonaCliente implements Serializable{
    
    private PersonaDAO personaDAO = new PersonaDAO(Persona.class);
    private PersonaClienteDAO pClientDAO = new PersonaClienteDAO(PersonaCliente.class);
    private PersonaCliente personaCliente;
    private Persona persona;
    private List<Persona> listaPersonas;

    public PersonaCliente getPersonaCliente() {
        return personaCliente;
    }

    public void setPersonaCliente(PersonaCliente personaCliente) {
        this.personaCliente = personaCliente;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public List<Persona> getListaPersonas() {
        return listaPersonas;
    }

    public void setListaPersonas(List<Persona> listaPersonas) {
        this.listaPersonas = listaPersonas;
    }
      
}
