/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taller.contr;

import com.taller.impl.PruebaDAO;
import com.taller.impl.TipoPruebaDAO;
import com.taller.models.Prueba;
import com.taller.models.TipoPrueba;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author fredy.romerousam
 */
@ManagedBean(name = "pruebas")
@SessionScoped
public class ManejoPrueba implements Serializable {

    private PruebaDAO pDAO = new PruebaDAO(Prueba.class);
    private TipoPruebaDAO tpDAO = new TipoPruebaDAO(TipoPrueba.class);
    private Prueba prueba;
    private TipoPrueba tprueba;
    private List<TipoPrueba> listarTipoP;
    private List<Prueba> listarPrueba;

    public Prueba getPrueba() {
        return prueba;
    }

    public void setPrueba(Prueba prueba) {
        this.prueba = prueba;
    }

    public TipoPrueba getTprueba() {
        return tprueba;
    }

    public void setTprueba(TipoPrueba tprueba) {
        this.tprueba = tprueba;
    }

    public List<TipoPrueba> getListarTipoP() {
        listarTipoP = tpDAO.findAll();
        return listarTipoP;
    }

    public void setListarTipoP(List<TipoPrueba> listarTipoP) {
        this.listarTipoP = listarTipoP;
    }

    public List<Prueba> getListarPrueba() {
        return listarPrueba;
    }

    public void setListarPrueba(List<Prueba> listarPrueba) {
        this.listarPrueba = listarPrueba;
    }

    @PostConstruct
    public void init() {
        prueba = new Prueba();
        tprueba = new TipoPrueba();
    }

}
