package com.taller.contr;

import com.taller.impl.TelefonosDAO;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;

@Named(value = "telefonos")
@SessionScoped
public class Telefonos implements Serializable {

    private TelefonosDAO telefonosDAO = new TelefonosDAO(Telefonos.class);
    private Telefonos telefonos;
    private List<Telefonos> listaTelefonos;

    public Telefonos getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(Telefonos telefonos) {
        this.telefonos = telefonos;
    }

    public List<Telefonos> getListaTelefonos() {
        return listaTelefonos;
    }

    public void setListaTelefonos(List<Telefonos> listaTelefonos) {
        this.listaTelefonos = listaTelefonos;
    }

}
