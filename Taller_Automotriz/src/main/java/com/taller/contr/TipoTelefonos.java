package com.taller.contr;

import com.taller.impl.TipoTelefonosDAO;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;

@Named(value = "tipo_telefonos")
@SessionScoped
public class TipoTelefonos implements Serializable {

    private TipoTelefonosDAO tipoTelefonosDAO = new TipoTelefonosDAO(TipoTelefonos.class);
    private TipoTelefonos tipoTelefonos;
    private List<TipoTelefonos> listaTipotel;

    public TipoTelefonos getTipoTelefonos() {
        return tipoTelefonos;
    }

    public void setTipoTelefonos(TipoTelefonos tipoTelefonos) {
        this.tipoTelefonos = tipoTelefonos;
    }

    public List<TipoTelefonos> getListaTipotel() {
        return listaTipotel;
    }

    public void setListaTipotel(List<TipoTelefonos> listaTipotel) {
        this.listaTipotel = listaTipotel;
    }

}
