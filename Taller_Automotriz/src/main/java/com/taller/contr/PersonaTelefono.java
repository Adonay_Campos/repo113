package com.taller.contr;

import com.taller.impl.PersonaDAO;
import com.taller.impl.PersonaTelefonoDAO;
import com.taller.impl.TelefonosDAO;
import com.taller.models.Persona;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;

@Named(value = "persona_telfono")
@SessionScoped
public class PersonaTelefono implements Serializable {

    private PersonaTelefonoDAO personaTelefonoDAO = new PersonaTelefonoDAO(PersonaTelefono.class);
    private PersonaDAO personaDAO = new PersonaDAO(Persona.class);
    private TelefonosDAO telefonosDAO = new TelefonosDAO(Telefonos.class);
    private Telefonos telefonos;
    private Persona persona;
    private PersonaTelefono personaTelefono;
    private List<PersonaTelefono> listaPersonaTel;

    public Telefonos getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(Telefonos telefonos) {
        this.telefonos = telefonos;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public PersonaTelefono getPersonaTelefono() {
        return personaTelefono;
    }

    public void setPersonaTelefono(PersonaTelefono personaTelefono) {
        this.personaTelefono = personaTelefono;
    }

    public List<PersonaTelefono> getListaPersonaTel() {
        return listaPersonaTel;
    }

    public void setListaPersonaTel(List<PersonaTelefono> listaPersonaTel) {
        this.listaPersonaTel = listaPersonaTel;
    }

}
