/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taller.contr;

import com.taller.impl.AutosDAO;
import com.taller.models.Autos;
import com.taller.models.Modelos;
import com.taller.models.TipoAutos;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author erick.gomezusam
 */
@Named(value = "autosA")
@ManagedBean
@SessionScoped
public class AutosA implements Serializable {

    private AutosDAO autosDAO = new AutosDAO(Autos.class);
    private Autos autos;
    private Modelos modelos;
    private TipoAutos tipoautos;
    private List<Autos> listaAutos;
    String mensaje="";

    public Autos getAutos() {
        return autos;
    }

    public void setAutos(Autos autos) {
        this.autos = autos;
    }

    public Modelos getModelos() {
        return modelos;
    }

    public void setModelos(Modelos modelos) {
        this.modelos = modelos;
    }

    public TipoAutos getTipoautos() {
        return tipoautos;
    }

    public void setTipoautos(TipoAutos tipoautos) {
        this.tipoautos = tipoautos;
    }

    public List<Autos> getListaAutos() {
        return listaAutos;
    }

    public void setListaAutos(List<Autos> listaAutos) {
        this.listaAutos = listaAutos;
    }
    
    @PostConstruct
    public void init() {
        this.autos = new Autos();
        this.modelos = new Modelos();
        this.tipoautos = new TipoAutos();
    }

    public void crear() {
        try {
            this.autos.setTipo(tipoautos);
            this.autos.setModelo(modelos);
            this.autosDAO.create(autos);
            this.mensaje = "Datos creados con exito";
        } catch (Exception e) {
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void editar() {
        try {
            this.autos.setTipo(tipoautos);
            this.autos.setModelo(modelos);
            this.autosDAO.edit(autos);
            this.mensaje = "Datos editados con exito";
        } catch (Exception e) {
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void eliminar(Autos a) {
        try {
            this.autosDAO.remove(a);
            this.mensaje = "Datos eliminados con exito";
        } catch (Exception e) {
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void cargarMarcas(Autos a){
        modelos.setId(a.getModelo().getId());
        tipoautos.setId(a.getTipo().getId());
        this.autos=a;
    }
    
}
