package com.taller.contr;

import com.taller.impl.EmpresaDAO;
import java.io.Serializable;
import java.util.List;

public class Empresa implements Serializable {

    private EmpresaDAO empresaDAO = new EmpresaDAO(Empresa.class);
    private Empresa empresa;
    private List<Empresa> listaEmpresa;

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public List<Empresa> getListaEmpresa() {
        return listaEmpresa;
    }

    public void setListaEmpresa(List<Empresa> listaEmpresa) {
        this.listaEmpresa = listaEmpresa;
    }

}
