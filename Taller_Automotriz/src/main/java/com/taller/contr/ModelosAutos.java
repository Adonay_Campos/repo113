/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.taller.contr;

import com.taller.impl.ModelosDAO;
import com.taller.models.Marcas;
import com.taller.models.Modelos;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

/**
 *
 * @author erick.gomezusam
 */
@Named(value = "modelosAutos")
@ManagedBean
@SessionScoped
public class ModelosAutos implements Serializable {

    private ModelosDAO modelosDAO = new ModelosDAO(Modelos.class);
    private Modelos modelos;
    private Marcas marcas;
    private List<Modelos> listaModelos;
    String mensaje="";

    public Modelos getModelos() {
        return modelos;
    }

    public void setModelos(Modelos modelos) {
        this.modelos = modelos;
    }

    public List<Modelos> getListaModelos() {
        return listaModelos;
    }

    public Marcas getMarcas() {
        return marcas;
    }

    public void setMarcas(Marcas marcas) {
        this.marcas = marcas;
    }

    public void setListaModelos(List<Modelos> listaModelos) {
        this.listaModelos = listaModelos;
    }
    
    @PostConstruct
    public void init() {
        this.modelos = new Modelos();
        this.marcas = new Marcas();
    }

    public void crear() {
        try {
            this.modelos.setMarca(marcas);
            this.modelosDAO.create(modelos);
            this.mensaje = "Datos creados con exito";
        } catch (Exception e) {
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void editar() {
        try {
            this.modelos.setMarca(marcas);
            this.modelosDAO.edit(modelos);
            this.mensaje = "Datos editados con exito";
        } catch (Exception e) {
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void eliminar(Modelos m) {
        try {
            this.modelosDAO.remove(m);
            this.mensaje = "Datos eliminados con exito";
        } catch (Exception e) {
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void cargarMarcas(Modelos m){
        marcas.setId(m.getMarca().getId());
        this.modelos=m;
    }   
}
