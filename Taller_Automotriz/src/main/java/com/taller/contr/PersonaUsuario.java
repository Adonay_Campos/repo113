package com.taller.contr;

import com.taller.impl.PersonaDAO;
import com.taller.impl.UserDAO;
import com.taller.models.Persona;
import com.taller.models.User;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@ManagedBean
@SessionScoped
public class PersonaUsuario implements Serializable {

    private PersonaDAO personaDAO = new PersonaDAO(Persona.class);
    private UserDAO userDAO = new UserDAO(User.class);
    private Persona persona;
    private User user;
    private String pass;
    private String us;
    private List<User> listaUser;
    private List<Persona> listaPersona;

    @PostConstruct
    public void init() {
        persona = new Persona();
        user = new User();
    }

    public String iniciarSesion() {
        FacesContext fc = FacesContext.getCurrentInstance();
        user = userDAO.iniciar(us, pass);
        if (user != null) {
            fc.getExternalContext().getSessionMap().put("us", user);
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "¡Bienvenido!", "Ha iniciado sesion con exito"));
            return "inicio?faces-redirect=true";
        } else {
            fc.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "¡Contraseña y Usuario Incorrectos!", "No esta registrado, verifique si su contraseña y su usuario son correctos"));
            return "index?faces-redirect=false";
        }
    }
    
    public String cerrarSesion(){
        FacesContext contex = FacesContext.getCurrentInstance();
		user = (User) contex.getExternalContext().getSessionMap().get("us");
		contex.getExternalContext().invalidateSession();
		return "index?faces-redirect=true";
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    //Lista de Usuarios
    public List<User> getListaUser() {
        listaUser = userDAO.findAll();
        return listaUser;
    }

    public void setListaUser(List<User> listaUser) {
        this.listaUser = listaUser;
    }

    //Lista de Perosnas
    public List<Persona> getListaPersona() {
        listaPersona = personaDAO.findAll();
        return listaPersona;
    }

    public void setListaPersona(List<Persona> listaPersona) {
        this.listaPersona = listaPersona;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getUs() {
        return us;
    }

    public void setUs(String us) {
        this.us = us;
    }

}