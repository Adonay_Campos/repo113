package com.taller.contr;

import com.taller.impl.MarcasDAO;
import com.taller.models.Marcas;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

@Named(value = "marcasAutos")
@ManagedBean
@SessionScoped
public class MarcasAutos implements Serializable {

    private MarcasDAO marcasDAO = new MarcasDAO(Marcas.class);
    private Marcas marcas;
    private List<Marcas> listaMarcas;
    String mensaje = "";

    public Marcas getMarcas() {
        return marcas;
    }

    public void setMarcas(Marcas marcas) {
        this.marcas = marcas;
    }

    public List<Marcas> getListaMarcas() {
        return listaMarcas;
    }

    public void setListaMarcas(List<Marcas> listaMarcas) {
        this.listaMarcas = listaMarcas;
    }

    @PostConstruct
    public void init() {
        this.marcas = new Marcas();
    }

    public void crear() {
        try {
            this.marcasDAO.create(marcas);
            this.mensaje = "Datos creados con exito";
        } catch (Exception e) {
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void editar() {
        try {
            this.marcasDAO.edit(marcas);
            this.mensaje = "Datos editados con exito";
        } catch (Exception e) {
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }
    public void eliminar(Marcas m) {
        try {
            this.marcasDAO.remove(m);
            this.mensaje = "Datos eliminados con exito";
        } catch (Exception e) {
            this.mensaje="Error: "+e.getMessage();
            e.printStackTrace();
        }
        FacesMessage msj = new FacesMessage(this.mensaje);
        FacesContext.getCurrentInstance().addMessage(null, msj);
    }public void cargarMarcas(Marcas m){
        this.marcas=m;
    }
}
