package com.taller.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-10-22T15:57:13")
@StaticMetamodel(TipoPrueba.class)
public class TipoPrueba_ { 

    public static volatile SingularAttribute<TipoPrueba, String> descripcion;
    public static volatile SingularAttribute<TipoPrueba, Integer> id;
    public static volatile SingularAttribute<TipoPrueba, String> nombre;

}