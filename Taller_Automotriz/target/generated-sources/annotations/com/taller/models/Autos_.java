package com.taller.models;

import com.taller.models.Modelos;
import com.taller.models.TipoAutos;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-10-22T15:57:13")
@StaticMetamodel(Autos.class)
public class Autos_ { 

    public static volatile SingularAttribute<Autos, TipoAutos> tipo;
    public static volatile SingularAttribute<Autos, String> color;
    public static volatile SingularAttribute<Autos, Integer> id;
    public static volatile SingularAttribute<Autos, Modelos> modelo;
    public static volatile SingularAttribute<Autos, String> placa;

}