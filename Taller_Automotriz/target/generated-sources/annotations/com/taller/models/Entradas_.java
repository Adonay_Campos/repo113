package com.taller.models;

import com.taller.models.Autos;
import com.taller.models.PersonaCliente;
import java.sql.Time;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-10-22T15:57:13")
@StaticMetamodel(Entradas.class)
public class Entradas_ { 

    public static volatile SingularAttribute<Entradas, Date> fecha;
    public static volatile SingularAttribute<Entradas, Character> estado;
    public static volatile SingularAttribute<Entradas, Autos> auto;
    public static volatile SingularAttribute<Entradas, Time> hora;
    public static volatile SingularAttribute<Entradas, PersonaCliente> persona_Cliente;
    public static volatile SingularAttribute<Entradas, Integer> id;

}