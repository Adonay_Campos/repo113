package com.taller.models;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-10-22T15:57:13")
@StaticMetamodel(TipoAutos.class)
public class TipoAutos_ { 

    public static volatile SingularAttribute<TipoAutos, String> descripcion;
    public static volatile SingularAttribute<TipoAutos, Integer> id;
    public static volatile SingularAttribute<TipoAutos, String> nombre;

}