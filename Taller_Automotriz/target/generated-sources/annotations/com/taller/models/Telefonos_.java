package com.taller.models;

import com.taller.models.TipoTelefonos;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-10-22T15:57:13")
@StaticMetamodel(Telefonos.class)
public class Telefonos_ { 

    public static volatile SingularAttribute<Telefonos, TipoTelefonos> tipo_telefonos;
    public static volatile SingularAttribute<Telefonos, String> numero;
    public static volatile SingularAttribute<Telefonos, Integer> id;

}