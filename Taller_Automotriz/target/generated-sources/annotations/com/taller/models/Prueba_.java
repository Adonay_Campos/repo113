package com.taller.models;

import com.taller.models.Entradas;
import com.taller.models.TipoPrueba;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-10-22T15:57:13")
@StaticMetamodel(Prueba.class)
public class Prueba_ { 

    public static volatile SingularAttribute<Prueba, Date> fecha;
    public static volatile SingularAttribute<Prueba, String> estado;
    public static volatile SingularAttribute<Prueba, TipoPrueba> tipo;
    public static volatile SingularAttribute<Prueba, Entradas> entrada;
    public static volatile SingularAttribute<Prueba, Integer> id;

}