package com.taller.models;

import com.taller.models.Marcas;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-10-22T15:57:13")
@StaticMetamodel(Modelos.class)
public class Modelos_ { 

    public static volatile SingularAttribute<Modelos, Marcas> marca;
    public static volatile SingularAttribute<Modelos, Integer> id;
    public static volatile SingularAttribute<Modelos, String> nombre;

}