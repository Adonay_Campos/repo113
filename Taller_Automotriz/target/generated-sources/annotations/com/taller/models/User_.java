package com.taller.models;

import com.taller.models.Persona;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-10-22T15:57:13")
@StaticMetamodel(User.class)
public class User_ { 

    public static volatile SingularAttribute<User, Byte> password;
    public static volatile SingularAttribute<User, Boolean> estado;
    public static volatile SingularAttribute<User, Persona> persona;
    public static volatile SingularAttribute<User, Integer> id;
    public static volatile SingularAttribute<User, String> user;

}