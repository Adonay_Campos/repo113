package com.taller.models;

import com.taller.models.Empresa;
import com.taller.models.Persona;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-10-22T15:57:13")
@StaticMetamodel(PersonaCliente.class)
public class PersonaCliente_ { 

    public static volatile SingularAttribute<PersonaCliente, Date> fecha;
    public static volatile SingularAttribute<PersonaCliente, Persona> persona;
    public static volatile SingularAttribute<PersonaCliente, Integer> id;
    public static volatile SingularAttribute<PersonaCliente, Empresa> empresa;

}