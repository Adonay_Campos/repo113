package com.taller.models;

import com.taller.models.Persona;
import com.taller.models.Telefonos;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2019-10-22T15:57:13")
@StaticMetamodel(PersonaTelefono.class)
public class PersonaTelefono_ { 

    public static volatile SingularAttribute<PersonaTelefono, Persona> persona;
    public static volatile SingularAttribute<PersonaTelefono, Integer> id;
    public static volatile SingularAttribute<PersonaTelefono, Telefonos> telefonos;

}